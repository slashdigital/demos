import subprocess
import os

def bow_extract(dataset, khmer):
    dico = []
    classtooutput = {}
    index = 0

    #Fill the dictionnary with every word from the database
    for elm in dataset['intents']:
        classtooutput.update({elm['class'] : index})
        index += 1

        for sentence in elm['sentence']:
            if khmer == 1:
                sentence = split(sentence)
            for words in sentence.split(" "):
                dico.append(words.lower())
    X = []
    Y = []
    dico = list(set(dico))

    for elm in dataset['intents']:
        for sentence in elm['sentence']:
            bow = [0] * len(dico)
            classes = [0] * index
            if khmer == 1:
                sentence = split(sentence)
            for words in sentence.split(" "):
                #Bow creation
                bow[dico.index(words.lower())] = 1

                #Output creation
                classes[classtooutput[elm['class']]] = 1
            X.append(bow)
            Y.append(classes)

    return X, Y, classtooutput, dico

def split(sentence):
    os.chdir('km-5tag-seg-1.0/')
    with open('nosplit.txt', 'wb') as f:
            f.write(sentence.encode())
    FNULL = open(os.devnull, 'w')
    subprocess.run(["bash", "../km-5tag-seg-test.sh", "../model/km-5tag-seg-model" ,"nosplit.txt", "."], stdout=FNULL, stderr=subprocess.STDOUT)
    with open('nosplit.txt.w') as f:
             txt = f.read()
    os.chdir('..')
    return txt

def simple_extract(sent, dico):
    X = [0] * len(dico)
    for words in sent.split(" "):
            try :
                    X[dico.index(words.lower())] = 1
            except:
                    continue
    return X
