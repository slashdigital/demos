import tflearn
import json
from feature import simple_extract, split
import sys
import tensorflow

def khmer_res(words):

    with open('.dico-khmer.json', 'r') as f:
        js_khmer = json.load(f)
        dico_khmer = js_khmer["dico"]
        classes = js_khmer["classes"]
    with tensorflow.Graph().as_default():
        #Creation of the neural network
        k_nn = tflearn.input_data(shape=[None, len(dico_khmer)])
        k_nn1 = tflearn.fully_connected(k_nn, 80)
        k_nn2 = tflearn.dropout(k_nn1, 0.3)
        k_nn3 = tflearn.fully_connected(k_nn2, 9, activation='softmax')
        k_nn4 = tflearn.regression(k_nn3, learning_rate=0.001,loss='mean_square')

        model_khmer = tflearn.DNN(k_nn4)

        model_khmer.load(model_file='./model/demo-khmer.tflearn')

        words = split(words)
        feature = simple_extract(words, dico_khmer)
        res = model_khmer.predict([feature])
        final = []
        for tag, val in zip(classes, *res):
            final.append((tag, val))
        final = sorted(final, key=lambda final : final[1], reverse=True)
        return final

#Compute the result
def result(words):
    with open('.dico.json', 'r') as f:
        js = json.load(f)
        dico = js["dico"]
        classes = js["classes"]

    with tensorflow.Graph().as_default():
        #Creation of the neural network
        nn = tflearn.input_data(shape=[None, len(dico)])
        nn1 = tflearn.fully_connected(nn, 80)
        nn2 = tflearn.dropout(nn1, 0.3)
        nn3 = tflearn.fully_connected(nn2, 9, activation='softmax')
        nn4 = tflearn.regression(nn3, learning_rate=0.001,loss='mean_square')

        model = tflearn.DNN(nn4)

        #Load the trained model into the new model
        model.load(model_file='./model/demo.tflearn')

        feature = simple_extract(words, dico)
        res = model.predict([feature])
        final = []
        for tag, val in zip(classes, *res):
            final.append((tag, val))
        final = sorted(final, key=lambda final : final[1], reverse=True)
        return final
