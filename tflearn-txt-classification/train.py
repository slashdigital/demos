import tflearn
import json
import argparse
from feature import *

ap = argparse.ArgumentParser()
ap.add_argument('-database', default='en_db.json')
ap.add_argument('-epochs', default=500)
args = vars(ap.parse_args())

khmer = 0
if args['database'] != "en_db.json":
    khmer = 1

#Load and preprocess the data
with open(args['database']) as f:
    data = json.load(f)

#Extract the feature from the data
Xinput, Youtput, classes, dico = bow_extract(data, khmer)

#Creation of the neural network
nn = tflearn.input_data(shape=[None, len(dico)])
nn1 = tflearn.fully_connected(nn, 80)
nn2 = tflearn.dropout(nn1, 0.3)
nn3 = tflearn.fully_connected(nn2, 9, activation='softmax')
nn4 = tflearn.regression(nn3, learning_rate=0.001,loss='mean_square')

#Create and Train the model
model = tflearn.DNN(nn4)
model.fit(Xinput, Youtput, args['epochs'], show_metric=True, batch_size=64)

#Save model for production
js = {"dico" : dico, "classes" : classes}
if khmer:
    model.save('./model/demo-khmer.tflearn')
    #Save dictionnary for classification
    with open('.dico-khmer.json', 'w') as f:
        json.dump(js, f)
else:
    model.save('./model/demo.tflearn')
    #Save dictionnary for classification
    with open('.dico.json', 'w') as f:
        json.dump(js, f)


