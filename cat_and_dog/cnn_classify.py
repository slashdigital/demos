from torch.autograd import Variable
from torch import nn
import torch
from network import CNN
from network import IMG_SIZE
import cv2
def classify(img_path):
	#Preprocessing on the image
	img = cv2.imread(img_path)
	img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	img.resize(1, 1,IMG_SIZE, IMG_SIZE)
	#load the trained model
	cnn = CNN()
	cnn.load_state_dict(torch.load('cnn.pt'))

	input = Variable(torch.FloatTensor(img.tolist()))
	cnn.eval()
	output = cnn(input)[0]
	pred = torch.max(output, 1)[1].data.squeeze()
	if pred[0] < 0.5:
		return "cat"
	else:
		return "dog"

print(classify("cat.jpg"))
