from flask import Flask, redirect, url_for, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
import os
from cnn_classify import classify

@app.route("/get_pred", methods=["POST", "OPTIONS"])
def predict():
	image_file = request.files.get("image", "")
	print("image_file: ", image_file)
	print(type(image_file))
	if image_file != "":
		image_file.save("image.jpg")
		prediction = classify("image.jpg")
		os.remove("image.jpg")
		print(prediction)
		return prediction
	return "Not Ok"
