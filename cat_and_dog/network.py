from torch import nn
IMG_SIZE = 24
class CNN(nn.Module):
	def __init__(self):
		super(CNN, self).__init__()
		self.conv1 = nn.Sequential(
			nn.Conv2d(
				in_channels=1,
				out_channels=32,
				kernel_size=5,
				stride=1,
				padding=2,
			),
			nn.ReLU(),
			nn.MaxPool2d(kernel_size=2),
			nn.Dropout(0.2),
		)
		self.conv2 = nn.Sequential(
			nn.Conv2d(32, 64, 5, 1, 2),
			nn.ReLU(),
			nn.MaxPool2d(2),
			nn.Dropout(0.2),
		)
		self.out = nn.Linear(2304, 2)

	def forward(self, x):
		x = self.conv1(x)
		x = self.conv2(x)
		x = x.view(x.size(0), -1)
		output = self.out(x)
		return output, x


	"""
	class Example(nn.Module):
		def __init__(CNN, self):
			super(CNN, self).__init__()
			self.input = nn.Linear(size_input, 4)
			self.output = nn.Linear(4, size_output)
		def forward(self, x):
			x = self.input(x)
			output = self.ouput(x)
			return output
	"""
