from torch.autograd import Variable
from torch import nn
import torchvision.transforms as transforms
from network import CNN
from network import IMG_SIZE 
import cv2
import torch
import os
import random
import numpy as np
trans = transforms.Normalize((0.0,),(1.0,))
dataset = []
LR = 0.00001
epochs = 40
index = 0
#Open Image/ apply prepocessing
for img_name in os.listdir("data/"):
	#if index == 10: break
	img = cv2.imread("data/" + img_name)
	#print(img_name)
	#cv2.fastNlMeansDenoising(img, img)
	img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	img.resize(1, IMG_SIZE, IMG_SIZE)
	if "dog" in img_name:
		dataset.append((img, 1))
	if "cat" in img_name:
		dataset.append((img, 0))
	index += 1

print("preprocessing finished")
#Separate Test and Training Data
train_set = dataset[:int(len(dataset) * 0.90)]
random.shuffle(train_set)
test_set = dataset[int(len(dataset) * 0.90):]

def generate_batchs(nb, train_set):
	batch_x = []
	batch_y = []
	X, Y = [], []
	for i in range(0,len(train_set)):
		X.append(train_set[i][0].tolist())
		Y.append(train_set[i][1])
		if i % nb == 0 and i != 0:
			#Add the current batch to the vector of batch
			X = Variable(trans(torch.FloatTensor(X)))
			Y = Variable(torch.LongTensor(Y))
			batch_x.append(X)
			batch_y.append(Y)
			X, Y = [], []
	if len(X) != 0: #Add the last batch to the vector of batch
		X = Variable(trans(torch.FloatTensor(X)))
		Y = Variable(torch.LongTensor(Y))
		batch_x.append(X)
		batch_y.append(Y)
	return batch_x, batch_y

#Generate batch of 30 element from the train_set
batch_x, batch_y  = generate_batchs(30, train_set)
cnn = CNN()
#Optimize learning rate value through the training
optimizer = torch.optim.Adam(cnn.parameters(), lr=LR)
#define how we are going to compute the error
loss_func = nn.CrossEntropyLoss()
j = 0
loss = None
#Train Loop
for i in range(epochs):
	for x, y in zip(batch_x, batch_y):
		#Compute each layer
		output = cnn(x)[0]
		#Compute the error(output vs expected)
		loss = loss_func(output, y)
		#reset optimizer gradient
		optimizer.zero_grad()
		#Backpropagate
		loss.backward()
		optimizer.step()
		j += 1
	print(loss.data.numpy(), "At epoch nb: ", i)
#Model goes from training mode to eval mode
cnn.eval()

#generate batchs 
test_x, test_y = generate_batchs(50, test_set)
false = 0

#Benchmarking
for x, y in zip(test_x, test_y):
	test_output, last_layer = cnn(x)
	pred_y = torch.max(test_output, 1)[1].data.squeeze()
	for i in range(pred_y.size(0)):
		val = 1
		if pred_y[i] < 0.5:
			val = 0
		if val != y[i].data.numpy()[0]:
			false += 1
#Print result
print(false, " / ", len(test_set))
print(false * 100 / len(test_set))
#Save the model
torch.save(cnn.state_dict(), "cnn.pt")
