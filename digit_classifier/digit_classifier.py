from torch import nn
import sys
import torch
import numpy
from torch.autograd import Variable
import random
from numpy import genfromtxt
import torchvision.transforms as transforms

class myNN(nn.Module):
	def __init__(self, size_input, size_output):
		super(myNN, self).__init__()
		self.layer1 = nn.Sequential(nn.Linear(size_input, 400), nn.Dropout(0.2), nn.ReLU(),)
		self.layer2 = nn.Sequential(nn.Linear(400, 200), nn.Dropout(0.2), nn.ReLU(),)
		self.layer3 = nn.Sequential(nn.Linear(200, size_output), nn.Softmax(),)
	def forward(self, x):
		x = self.layer1(x)
		x = self.layer2(x)
		output = self.layer3(x)
		return output

if len(sys.argv) == 1:

	data = genfromtxt("train_small.csv", delimiter=",")
	data = data[1:]
	random.seed(10)
	random.shuffle(data)
	X = []
	Y = []
	for elm in data:
		y  = [0] * 10
		y[int(elm[0])] = 1
		Y.append(y)
		X.append(elm[1:])

	store_x = X
	store_y = Y
	X = store_x[:int(len(X) * 0.75)]
	Y = store_y[:int(len(Y) * 0.75)]
	test_x = store_x[int(len(store_x) * 0.75):]
	test_y = store_y[int(len(store_y) * 0.75):]


	my_nn = myNN(len(X[0]), 10)
	trans = transforms.Normalize((0.5,), (1.0,))

	X = Variable(trans(torch.FloatTensor(X)))
	Y = Variable(torch.FloatTensor(Y))

	learning_rate = 0.0001
	iteration = 350
	loss_func = nn.MSELoss()
	optimizer = torch.optim.Adam(my_nn.parameters(), lr=learning_rate)

	loss_history = []
	print(len(X))
	print(len(X[0]))
	for i in range(iteration):
		output = my_nn(X)
		loss = loss_func(output, Y)
		optimizer.zero_grad()
		loss.backward()
		loss_history.append(loss.data.numpy()[0])
		optimizer.step()
		if i % 50 == 0:
			print("rate ", loss.data.numpy()[0], "iteration", i)

	torch.save(my_nn.state_dict(), "model.pt")
	my_nn.eval()

	import matplotlib.pyplot as plt
	plt.plot(loss_history)
	plt.ylabel("Error Rate")
	plt.show()

	test_x = Variable(trans(torch.FloatTensor(test_x)))
	error = 0
	output = my_nn(test_x)
	for elm, expected in  zip(output, test_y):
		elm = elm.data.numpy()
		if numpy.argmax(elm) != expected.index(1):
			error += 1
	print(error * 100 / len(test_y), "%")
	print(len(test_y))

else:
	my_nn = myNN(28 * 28, 10)
	my_nn.load_state_dict(torch.load("good_model.pt"))
	my_nn.eval()
	trans = transforms.Normalize((0.5,), (1.0,))
	import cv2
	images = ["img/2.png"]
	for image in images:
		img = cv2.imread(image)
		img = cv2.resize(img, (28, 28))
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		img.resize(28 * 28)

		img = Variable(trans(torch.FloatTensor([img.astype(float)])))
		output = my_nn(img)
		print("Output for image: ", image)
		print(numpy.argmax(output.data.numpy()))
